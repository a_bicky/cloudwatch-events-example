## 1.Create scheduled_task user

```
terraform init
terraform apply
```

## 2. Set credentials of scheduled_task user

```
export AWS_ACCESS_KEY_ID=XXXXXXXXXX
export AWS_SECRET_ACCESS_KEY=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
```

## 3. Register a scheduled task

```
AWS_ACCOUNT_ID=XXXXXXXXX bundle exec cap production ecs:deploy_scheduled_task
```
