# config valid only for current version of Capistrano
lock "3.10.1"

set :ecs_region, "ap-northeast-1"
set :ecs_tasks, [
  {
    name: "scheduled-task",
    container_definitions: [{
      image: "busybox",
      cpu: 256,
      memory: 100,
      essential: true,
      name: "scheduled-task-container",
    }]
  },
]

set :ecs_scheduled_tasks, [{
  cluster: ENV["CLUSTER"] || "default",
  rule_name: "scheduled-task-via-ecs-deploy",
  schedule_expression: "cron(* * * * ? *)",
  task_definition_name: "scheduled-task",
  role_arn: "arn:aws:iam::#{ENV["AWS_ACCOUNT_ID"]}:role/ecsEventsRole",
}]
