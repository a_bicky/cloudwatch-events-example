provider "aws" {
  region = "ap-northeast-1"
}

data "aws_caller_identity" "current" {}

locals {
  account_id = "${data.aws_caller_identity.current.account_id}"
  region = "ap-northeast-1"
}

resource "aws_iam_user" "scheduled_task" {
  name = "scheduled_task"
}

resource "aws_iam_access_key" "scheduled_task_key" {
  user = "${aws_iam_user.scheduled_task.name}"
}

resource "aws_iam_policy_attachment" "scheduled_task_policy_attachment" {
  name = "scheduled_task_policy_attachment"
  users = ["${aws_iam_user.scheduled_task.name}"]
  roles = []
  groups = []

  policy_arn = "${aws_iam_policy.scheduled_task_policy.arn}"
}

resource "aws_iam_policy" "scheduled_task_policy" {
  name        = "scheduled_task_policy"
  path        = "/"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ecs:RegisterTaskDefinition",
        "ecs:DescribeTaskDefinition"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "ecs:DescribeClusters"
      ],
      "Resource": [
        "arn:aws:ecs:${local.region}:${local.account_id}:cluster/default",
        "arn:aws:ecs:${local.region}:${local.account_id}:cluster/test"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "events:PutRule"
      ],
      "Resource": [
        "arn:aws:events:${local.region}:${local.account_id}:rule/scheduled-task-via-ecs-deploy"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "events:PutTargets"
      ],
      "Resource": [
        "arn:aws:events:${local.region}:${local.account_id}:rule/scheduled-task-via-ecs-deploy"
      ],
      "Condition": {
        "ArnEquals": {
          "events:TargetArn": "arn:aws:ecs:${local.region}:${local.account_id}:cluster/default"
        }
      }
    },
    {
      "Effect": "Allow",
      "Action": [
        "iam:PassRole"
      ],
      "Resource": "arn:aws:iam::${local.account_id}:role/ecsEventsRole"
    }
  ]
}
EOF
}
